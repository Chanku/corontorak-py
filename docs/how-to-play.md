#HOW TO PLAY  

##Starting the Game  
In order to start the game, you need to go into the corontorak folder and do the follwing in a command prompt:  
`python3 start.py`   
If you wish to do a multiplayer game you must either act as the server, or as the client. If you are connecting to a server do the following:  
`python3 start.py --client [ip of host] (port)`  
if the server owner didn't give you a port, then you can leave it blank  
If you wish to host a server simply do:  
`python3 start.py --server (port)`  
It is suggested you leave the port blank, however you may set one.

NOTE: If you are on windows you will need to do C:\path\to\python3 inplace of python3. 

##Basics  
CoronTorak, being a basic turn-based-strategy game, is very easy to grasp. Each player has 6 units. 2 Mages, 2 Warriors, and 2 Rangers/Archers.

There are differences between the classes. They are listed below:  
|      Unit     |  Range  | Health | Attack |   Strength    |   Weakness    |
|:-------------:|:-------:|:------:|:------:|:-------------:|:-------------:|
|      Mage     |   03    |   5    |   3    |    Warrior    | Ranger/Archer |  
| Ranger/Archer |   02    |   10   |   5    |     Mage      |   Warrior     | 
|    Warrior    |   01    |   10   |   6    | Ranger/Archer |     Mage      |

As you can see, Mages are the weakest unit in the game; however, they have the furthest range. Rangers/Archers are a medium unit, in all departments. Also Warriors have the smallest range, only being able to attack one around itself directly, however it is the strongest unit. Also when a unit has a weakness against a unit, it will loose one strength, if it has a strength against a unit it gains 2 strength, and if it's the same it gets no buff. It should be noted that the range is given in distance (in squares) from the unit. 

To move you can move one of two ways:  
1. Click on the unit and then click on any square next to the unit, that has no unit.
2. Click on the unit and then use the key-bindings to move the unit to a square. By default it uses WSAD. 

To do other actions look at the following:  
- To pass a turn hit the pass key (by default it is P)  
- To skip a unit (and count it as used) hit the skip key (default is e)  
- To reset press the reset key (default is r). THIS IS DISABLED IN MULTIPLAYER!

##Configuration  
CoronTorak Configuration is rather simple and easy. At the time configuration is only used for key-bindings. To modify them simply put the key-letter (a space must be put between the colon (:) and the value). If you wish to use a non-letter binding, please refer to the PyGame documentation for the string/name of that key.

##Saving/Loading  
Saving/Loading is not entirely implemented at this time. 

##Online  
Online is rather simple and easy. To do you either need to be a server, or connect to a server. To be a server do the following command:  
`python3 start.py server`  
This will start a server, on your computer, using the port 65522 . To use a different port do the following command: 
`python3 start.py server (port)`  
The where (port) is the numberical value of the port. For example opening a server on port 5000 do the following:  
`python3 start.py server 5000`  

Please note that using an alternative port will require the client to also know the port.

For connecting it is also easy. Simply do the following:  
`python3 start.py client (ip of host)`  
Where (ip of host) is the ip of the server. Since we didn't include a port it connected on the default port (65522). For example to connect to a host with the ip 192.168.3.5 on port 65522 do the following:  
`python3 start.py client 192.168.3.5`  
To connect to a server named ctserv.us.to on port 65522 do:  
`python3 start.py client ctserv.us.to`  

To connect to a server with a different port, include a space and write the port number. For example to connect to a server named ctserv.us.to on the port 5000 do:  
`python3 start.py client ctserv.us.to 5000`  
if the server had an ip of 192.168.3.5:  
`python3 start.py client 192.168.3.5 5000`

Please note that not including a port is the same as including the default port 65522!
