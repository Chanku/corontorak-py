#CoronTorak-Py  
##What is CoronTorak-Py?  
It is a port of CoronTorak, a game I developed for my final project in my programming class for my Sophomore Year in Visual Basic. It was also titled as Basic-TBS.

This is merely a Python port of the original game (not including my later additions to the game (Basically the current 2.0 Release Candidate (2.0 RC1))).

The Artwork was created by Dazeyy.

##Licensing
The artwork available here is licensed under the CC-BY-SA ![License](https://licensebuttons.net/1/by-sa/4.0/88x31.png)

The code is available under the MIT License. This can be found in the LICENSE file. 

##PFAQ  
###Will you be porting the latest developments?  
Maybe at a later time, however right now I just want to port the original game, and possibly add in a few improvements.

###Will this be developed at all?  
Probably not, aside from bug-fixes and some possible features.

###Will you support Networked Multiplayer this time?  
I am definitely considering it, and I do want to actually implement it this time. 

###What about AI?  
Maybe. I'm not exactly sure how to make a smart one though.

###How do I play?  
See the How-To-Play file in the docs/ folder
