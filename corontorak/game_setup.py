import pygame
import board
import units

def generate_gameboard(square_size=(70, 70), board_length=7, board_width=7, surface=None):
    square_length, square_width = square_size
    board_length_squares = 0
    board_width_squares = 0
    square_group = pygame.sprite.Group()
    while board_width_squares < board_width:
        square_group.add(board.square((board_length_squares * square_length),
                                      (board_width_squares * square_width), square_length,
                                      square_width, surface=surface))
        if board_length_squares < board_length and (board_length_squares + 1) < board_length:
            board_length_squares += 1
        else:
            board_length_squares = 0
            board_width_squares += 1
    return square_group

def place_units(row_setup, team='orange', invert=False, true_invert=False):
    group = pygame.sprite.Group()
    for row in row_setup:
        for col in row:
            if col == 'm':
                if not invert and not true_invert:
                    group.add(units.mage(row.index(col) * 70, row_setup.index(row) * 70, team))
                elif invert and not true_invert:
                    group.add(units.mage(row.index(col) * 70, (row_setup.index(row) + 5) * 70,
                                         team))
                elif true_invert:
                    group.add(units.mage(row.index(col) * 70, (6 - row_setup.index(row)) * 70,
                                         team))
                row.insert(row.index(col), 0)
                row.remove(col)
            elif col == 'w':
                if not invert:
                    group.add(units.warrior(row.index(col) * 70, row_setup.index(row) * 70, team))
                elif invert:
                    group.add(units.warrior(row.index(col) * 70, (row_setup.index(row) + 5) * 70,
                                            team))
                elif true_invert:
                    group.add(units.warrior(row.index(col) * 70, (6 - row_setup.index(row)) * 70,
                                            team))
                row.insert(row.index(col), 0)
                row.remove(col)
            elif col == 'a':
                if not invert:
                    group.add(units.ranger(row.index(col) * 70, row_setup.index(row) * 70, team))
                elif invert:
                    group.add(units.ranger(row.index(col) * 70, (row_setup.index(row) + 5) * 70,
                                           team))
                elif true_invert:
                    group.add(units.ranger(row.index(col) * 70, (6 - row_setup.index(row)) * 70,
                                           team))
                row.insert(row.index(col), 0)
                row.remove(col)
    return group
