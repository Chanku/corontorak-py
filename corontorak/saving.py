def _write_save(location):
    if location:
        pass

def load_game(location):
    try:
        f = open('{}/ct_data.sav', 'r')
        data = f.read()
        f.close()
    except OSError:
        f = open('{}/Corontorak_save.txt', 'r')
        data = f.read()
        f.close()
        convert_to_new(data)
        load_game(location)

def convert_to_new(data):
    f = open('{}/ct_data.sav', 'w')
    data = data.strip('2.0')
    turn = data.split('0 Player')[0][4:]
    if int(turn) % 2 == 0:
        f.write('orange')
    elif int(turn) % 2 == 1:
        f.write('blue')
