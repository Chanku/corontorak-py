import pygame

class square(pygame.sprite.Sprite):
    def __init__(self, x, y, height=70, width=70, image=None, surface=None):
        super().__init__()
        if image:
            self.image = pygame.image.load(image)
            self.rect = self.image.get_rect()
        else:
            self.image = pygame.draw.rect(surface, (173, 216, 230), (x, y, width, height), 2)
            self.rect = self.image
        self.rect.x = x
        self.rect.y = y
        self.x = x
        self.y = y
