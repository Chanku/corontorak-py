import socket
import pickle

class c_client():
    def __init__(self, connection_ip, connection_port, protocol=1):
        self.client = socket.socket()
        try:
            self.client.connect((connection_ip, int(connection_port)))
        except socket.error as e:
            print("unable to connect")
            print(str(e))
        self.client.settimeout(3)

    def send_state(self, state):
        self.client.send(pickle.dumps(state, -1))

    def send_command(self, command):
        self.client.send(command.encode('utf-8'))

    def shut_down(self):
        self.client.send('goodbye'.encode('utf-8'))
        self.client.close()

    def recv_state(self):
        return pickle.loads(self.client.recv(2048))

    def recv_command(self):
        return self.client.recv(2048).decode('utf-8')


class s_client():
    def __init__(self, server_ip, server_port, protocol=1):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print(server_ip)
        try:
            sock.bind((server_ip, int(server_port)))
        except socket.error as e:
            print('An error in binding the IP/PORT. Do you have another server running?')
            print(str(e))

        sock.listen(5)
        client, addr = sock.accept()
        self.client = client
        self.client.settimeout(3)

    def send_state(self, state):
        self.client.send(pickle.dumps(state, -1))
    
    def send_command(self, command):
        self.client.send(command.encode('utf-8'))

    def shut_down(self):
        self.client.send('goodbye'.encode('utf-8'))
        self.client.close()

    def recv_state(self):
        return pickle.loads(self.client.recv(2048))

    def recv_command(self):
        return self.client.recv(2048).decode('utf-8')
