def _write_config(location):
    config = open('{}/ct_config.txt'.format(location), 'w')
    config.write('#CoronTorak-Python Config File#\n'
                 '#1.0\n'
                 ':KEYS:\n'
                 'UP: w\n'
                 'DOWN: s\n'
                 'LEFT: a\n'
                 'RIGHT: d\n'
                 'PASS: p\n'
                 'RESET: r\n'
                 'SKIP: r\n'
                 'ATTACK: g\n')
    config.close()

def _load_base_config(location):
    config = open('{}/ct_config.txt'.format(location), 'r')
    data = config.read()
    config.close()
    return data

def _load_v1_config(data):
    key_list = data.split(':KEYS:\n')[1]
    key_list = key_list.split('\n')
    key_dict = {}
    for item in key_list:
        try:
            key = item.split(':')
            key_dict[key[0]] = key[1]
        except IndexError:
            pass
    return key_dict

def load_config(location):
    try:
        config_data = _load_base_config(location)
        check_version = config_data.split('#')[3][:4]
        if check_version == '1.0\n':
            return _load_v1_config(config_data)
    except OSError:
        _write_config(location)
        return load_config(location)
