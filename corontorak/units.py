import pygame

class unit(pygame.sprite.Sprite):
    def __init__(self, x, y, strength, health, image=None):
        super().__init__()
        self.image = pygame.image.load(image)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.x = x
        self.y = y
        self.strength = strength
        self.health = health

    def update(self, image=None):
        if image:
            self.image = pygame.image.load(image)

    def set_pos(self, x, y):
        self.rect.x = x
        self.rect.y = y
        self.x = x
        self.y = y

    def compare_image_straight(self, image_to_compare):
        return pygame.image.tostring(self.image, 'RGBA') == pygame.image.tostring(image_to_compage,
                                                                                  'RGBA')
    def package(self):
        return {'unit_type':type(self), 'unit_loc':(self.rect.x, self.rect.y), 
                'unit_health':self.health, 'unit_strength':self.strength}

    def unpackage(self, package):
        try:
            for unit_dict in package['units']:
                if str(unit_dict['unit_type']) == str(type(self)):
                    self.set_pos(unit_dict['unit_loc'][0], unit_dict['unit_loc'][1])
                    self.health = unit_dict['unit_health']
                    self.strength = unit_dict['unit_strength']
                    package['units'].remove(unit_dict)
                    return package
        except TypeError:
            pass

    def image_compare_picklable_unit(self, unit):
        return pygame.image.tostring(self.image, 'RGBA') == unit.image

class warrior(unit):
    def __init__(self, x, y, team='orange', strength=6, health=10, unit_range=1):
        if team == 'orange':
            team = 'orange_down'
        else:
            team = 'blue_up'
        super().__init__(x, y, strength, health, image='images/warrior_{}.png'.format(team))
        self.unit_range = unit_range

    def set_buffs(self, defending_unit='warrior'):
        if defending_unit == 'warrior':
            defending_unit = warrior
        if isinstance(defending_unit, ranger):
            self.strength += 2
        elif isinstance(defending_unit, mage):
            self.strength -= 1
        elif isinstance(defending_unit, warrior):
            self.strength = 6

class ranger(unit):
    def __init__(self, x, y, team='orange', strength=5, health=10, unit_range=2):
        if team == 'orange':
            team = 'orange_down'
        else:
            team = 'blue_up'
        super().__init__(x, y, strength, health, image='images/archer_{}.png'.format(team))
        self.unit_range = unit_range

    def set_buffs(self, defending_unit='ranger'):
        if defending_unit == 'ranger':
            defending_unit = ranger
        if isinstance(defending_unit, ranger):
            self.strength = 5
        elif isinstance(defending_unit, mage):
            self.strength += 2
        elif isinstance(defending_unit, warrior):
            self.strength -= 1

class mage(unit):
    def __init__(self, x, y, team='orange', strength=3, health=5, unit_range=3):
        if team == 'orange':
            team = 'orange_down'
        else:
            team = 'blue_up'
        super().__init__(x, y, strength, health, image='images/mage_{}.png'.format(team))
        self.unit_range = unit_range

    def set_buffs(self, defending_unit='mage'):
        if defending_unit == 'mage':
            defending_unit = mage
        if isinstance(defending_unit, ranger):
            self.strength -= 1
        elif isinstance(defending_unit, mage):
            self.strength = 3
        elif isinstance(defending_unit, warrior):
            self.strength += 2
