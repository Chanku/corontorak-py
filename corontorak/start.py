import math
import multiplayer
import pygame
import config
import socket_stuff
import socket
import sys
import time
from game_setup import *


VERSION = '1.0a'

def check_if_collide(current_group, opposing_group, current_unit):
    for unit in current_group:
        if unit.rect.collidepoint(current_unit.rect.x, current_unit.rect.y):
            if unit != current_unit:
                return True
    for unit in opposing_group:
        if unit.rect.collidepoint(current_unit.rect.x, current_unit.rect.y):
            if unit != current_unit:
                return True
    return False

def check_if_win(group1, group2):
    if len(group1.sprites()) <= 0:
        return True
    elif len(group2.sprites()) <= 0:
        return True
    else:
        return False

def multiplayer_loop(is_server):
    pygame.init()
    is_running = True
    h_w = (70*7,70*7)
    window = pygame.display.set_mode(h_w)
    clock = pygame.time.Clock()
    fps = 60
    clock.tick(fps)
    key_dict = config.load_config('configuration')
    orange_group = place_units([
        [0, 0, 'a', 'w', 'a', 0, 0],
        [0, 'm', 0, 'w', 0, 'm', 0]], team='orange', invert=False)
    blue_group = place_units([
        [0, 'm', 0, 'w', 0, 'm', 0],
        [0, 0, 'a', 'w', 'a', 0, 0]], team='blue', invert=True)
    burned_group = pygame.sprite.Group()
    pygame.display.update()
    current_mode = 'select'
    selected_unit = None
    current_group = blue_group
    opposing_group = orange_group
    square_group = generate_gameboard(surface=window)
    current_group_string = 'Blue'
    if is_server:
        try:
            client = multiplayer.s_client(socket_stuff.get_lan_ip(), sys.argv[2])
        except IndexError:
            client = multiplayer.s_client(socket_stuff.get_lan_ip(), 65522)
        is_your_turn = True

    elif not is_server:
        try:
            client = multiplayer.c_client(sys.argv[2], sys.argv[3])
        except IndexError:
            client = multiplayer.c_client(sys.argv[2], 65522)
        is_your_turn = False
    
    while is_running:
        pygame.display.set_caption(("CoronTorak-MPy v {} | Current Team: {} | Current Mode: {}"
                                   ).format(VERSION, current_group_string.title(), current_mode.title()))
        if is_your_turn:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                   client.shut_down()
                   is_running = False
                elif event.type == pygame.MOUSEBUTTONUP:
                    mouse_pos = pygame.mouse.get_pos()
                    if current_mode == 'select':
                        for unit in current_group.sprites():
                            if unit.rect.collidepoint(mouse_pos) and not burned_group.has(unit):
                                selected_unit = unit
                                current_mode = 'movement'
                    elif current_mode == 'movement':
                        for square in square_group.sprites():
                            if selected_unit:
                                if square.rect.collidepoint(mouse_pos) and not burned_group.has(unit):
                                    if (math.sqrt(((square.rect.x - selected_unit.rect.x)**2) +
                                                  ((square.rect.y - selected_unit.rect.y)**2)
                                                 )// 70) <= 1:
                                        old_coords = (selected_unit.rect.x, selected_unit.rect.y)
                                        selected_unit.set_pos(square.x, square.y)
                                        if check_if_collide(current_group, opposing_group,
                                                            selected_unit):
                                            selected_unit.set_pos(old_coords[0], old_coords[1])
                                        else:
                                            burned_group.add(selected_unit)
                                        selected_unit = None
                        current_mode = 'select'
                    elif current_mode == 'attack':
                        if selected_unit:
                            for unit in opposing_group.sprites():
                                if unit.rect.collidepoint(mouse_pos):
                                    if (math.sqrt(((unit.rect.x - selected_unit.rect.x)**2) +
                                                  ((unit.rect.y - selected_unit.rect.y)**2))//70
                                       ) <= selected_unit.unit_range:
                                        burned_group.add(selected_unit)
                                        current_mode = 'select'
                                        print("Unit Health: {}".format(unit.health))
                                        selected_unit.set_buffs(unit)
                                        unit.health -= (unit.strength)
                                        selected_unit.set_buffs()
                                        print("Unit Health: {}".format(unit.health))
                                        selected_unit = None
                        else:
                            for unit in current_group.sprites():
                                if unit.rect.collidepoint(mouse_pos) and not burned_group.has(unit):
                                    selected_unit = unit
                elif event.type == pygame.KEYDOWN:
                    for key in key_dict:
                        if pygame.key.name(event.key) == key_dict[key].strip(' '):
                            if 'pass' in key.lower():
                                #code for turn passing
                                burned_group.empty()
                                selected_unit = None
                                is_your_turn = False
                                current_mode = 'select'
                                if current_group == blue_group:
                                    current_group_string = 'Orange'
                                    current_group = orange_group
                                    opposing_group = blue_group
                                elif current_group == orange_group:
                                    current_group_string = 'Blue'
                                    current_group = blue_group
                                    opposing_group = orange_group
                                blue_list = []
                                orange_list = []
                                for unit in blue_group.sprites():
                                    blue_list.append(unit.package())
                                client.send_state({'effects':'team',
                                                   'package':{'team':'blue', 'units':blue_list}})
                                for unit in orange_group.sprites():
                                    orange_list.append(unit.package())
                                client.send_state({'effects':'team',
                                                   'package':{'team':'orange', 'units':orange_list}
                                                   })
                                for unit in blue_group.sprites():
                                    if unit.health <= 0:
                                        blue_group.remove(unit)
                                for unit in orange_group.sprites():
                                    if unit.health <= 0:
                                        orange_group.remove(unit)
                            elif 'skip' in key.lower():
                                #code for skipping a unit
                                if selected_unit:
                                    burned_group.add(selected_unit)
                                    current_mode = 'select'
                                    selected_unit = None
                            elif 'up' in key.lower():
                                #code for moving a unit up
                                if selected_unit:
                                    if not burned_group.has(selected_unit):
                                        selected_unit.set_pos(selected_unit.rect.x,
                                                              selected_unit.rect.y - 70)
                                        if check_if_collide(current_group, opposing_group,
                                                            selected_unit):
                                            selected_unit.set_pos(selected_unit.rect.x,
                                                                  selected_unit.rect.y + 70)
                                        else:
                                            burned_group.add(selected_unit)
                                            selected_unit = None
                            elif 'down' in key.lower():
                                #code for moving a unit down
                                if selected_unit:
                                    if not burned_group.has(selected_unit):
                                        selected_unit.set_pos(selected_unit.rect.x,
                                                              selected_unit.rect.y + 70)
                                        if check_if_collide(current_group, opposing_group,
                                                            selected_unit):
                                            selected_unit.set_pos(selected_unit.rect.x,
                                                                  selected_unit.rect.y - 70)
                                        else:
                                            burned_group.add(selected_unit)
                                            selected_unit = None
                            elif 'left' in key.lower():
                                #code for moving a unit to the left
                                if selected_unit:
                                    if not burned_group.has(selected_unit):
                                        selected_unit.set_pos(selected_unit.rect.x + 70,
                                                              selected_unit.rect.y)
                                        if check_if_collide(current_group, opposing_group,
                                                            selected_unit):
                                            selected_unit.set_pos(selected_unit.rect.x - 70,
                                                                  selected_unit.rect.y)
                                        else:
                                            burned_group.add(selected_unit)
                                            selected_unit = None
                            elif 'right' in key.lower():
                                #code for moving a unit to the right
                                if selected_unit:
                                    if not burned_group.has(selected_unit):
                                        selected_unit.set_pos(selected_unit.rect.x - 70,
                                                              selected_unit.rect.y)
                                        if check_if_collide(current_group, opposing_group,
                                                            selected_unit):
                                            selected_unit.set_pos(selected_unit.rect.x + 70,
                                                                  selected_unit.rect.y)
                                        else:
                                            burned_group.add(selected_unit)
                                            selected_unit = None
                            elif 'attack' in key.lower():
                                current_mode = 'attack'

            if burned_group.has(current_group):
                burned_group.empty()
                selected_unit = None
                is_your_turn = False
                current_mode = 'select'
                if current_group == blue_group:
                    current_group_string = 'Orange'
                    current_group = orange_group
                    opposing_group = blue_group
                elif current_group == orange_group:
                    current_group_string = 'Blue'
                    current_group = blue_group
                    opposing_group = orange_group
                    blue_list = []
                    orange_list = []
                for unit in blue_group.sprites():
                    blue_list.append(unit.package())
                    client.send_state({'effects':'team',
                                       'package':{'team':'blue', 'units':blue_list}})
                for unit in orange_group.sprites():
                    orange_list.append(unit.package())
                    client.send_state({'effects':'team',
                                       'package':{'team':'orange', 'units':orange_list}})
                for unit in blue_group.sprites():
                        if unit.health <= 0:
                            blue_group.remove(unit)
                for unit in orange_group.sprites():
                        if unit.health <= 0:
                            orange_group.remove(unit)
        else:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    client.shut_down()
                    is_running = False
            try:
                try:
                    blue_list = client.recv_state()['package']
                    orange_list = client.recv_state()['package']
                    for unit in blue_group.sprites():
                        blue_list = unit.unpackage(blue_list)
                    for unit in orange_group.sprites():
                        orange_list = unit.unpackage(orange_list)
                    if current_group == blue_group:
                        current_group_string = 'Orange'
                        current_group = orange_group
                        opposing_group = blue_group
                    elif current_group == orange_group:
                        current_group_string = 'Blue'
                        current_group = blue_group
                        opposing_group = orange_group
                    for unit in blue_group.sprites():
                        if unit.health <= 0:
                            blue_group.remove(unit)
                    for unit in orange_group.sprites():
                        if unit.health <= 0:
                            orange_group.remove(unit)
                    is_your_turn = True
                except socket.timeout:
                    pass
            except ValueError:
                is_running = False

        window.fill((0, 204, 0))
        square_group = generate_gameboard(surface=window)
        orange_group.draw(window)
        blue_group.draw(window)
        pygame.display.update()
        if check_if_win(blue_group, orange_group):
            is_running = False

def main_loop():
    pygame.init()
    is_running = True
    h_w = (70*7, 70*7)
    window = pygame.display.set_mode(h_w)
    pygame.display.set_caption(("CoronTorak-Py v {}  | Current Team: Blue | Current Mode: Select"
                               ).format(VERSION))
    clock = pygame.time.Clock()
    fps = 60
    clock.tick(fps)

    key_dict = config.load_config('configuration')
    orange_group = place_units([
        [0, 0, 'a', 'w', 'a', 0, 0],
        [0, 'm', 0, 'w', 0, 'm', 0]], team='orange', invert=False)
    blue_group = place_units([
        [0, 'm', 0, 'w', 0, 'm', 0],
        [0, 0, 'a', 'w', 'a', 0, 0]], team='blue', invert=True)
    burned_group = pygame.sprite.Group()
    pygame.display.update()
    current_mode = 'select'
    selected_unit = None
    current_group = blue_group
    opposing_group = orange_group
    square_group = generate_gameboard(surface=window)
    current_group_string = 'Blue'

    while is_running:
        pygame.display.set_caption(("CoronTorak-Py v {} | Current Team: {} | Current Mode: {}"
                                   ).format(VERSION, current_group_string.title(), current_mode.title()))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_running = False
            elif event.type == pygame.MOUSEBUTTONUP:
                mouse_pos = pygame.mouse.get_pos()
                if current_mode == 'select':
                    for unit in current_group.sprites():
                        if unit.rect.collidepoint(mouse_pos) and not burned_group.has(unit):
                            selected_unit = unit
                            current_mode = 'movement'
                elif current_mode == 'movement':
                    for square in square_group.sprites():
                        if selected_unit:
                            if square.rect.collidepoint(mouse_pos) and not burned_group.has(unit):
                                if (math.sqrt(((square.rect.x - selected_unit.rect.x)**2) +
                                              ((square.rect.y - selected_unit.rect.y)**2)
                                             )// 70) <= 1:
                                    old_coords = (selected_unit.rect.x, selected_unit.rect.y)
                                    selected_unit.set_pos(square.x, square.y)
                                    if check_if_collide(current_group, opposing_group,
                                                        selected_unit):
                                        selected_unit.set_pos(old_coords[0], old_coords[1])
                                    else:
                                        burned_group.add(selected_unit)
                                    selected_unit = None
                    current_mode = 'select'
                elif current_mode == 'attack':
                    if selected_unit:
                        for unit in opposing_group.sprites():
                            if unit.rect.collidepoint(mouse_pos):
                                if (math.sqrt(((unit.rect.x - selected_unit.rect.x)**2) +
                                              ((unit.rect.y - selected_unit.rect.y)**2))//70
                                   ) <= selected_unit.unit_range:
                                    burned_group.add(selected_unit)
                                    current_mode = 'select'
                                    print("Unit Health: {}".format(unit.health))
                                    selected_unit.set_buffs(unit)
                                    unit.health -= (unit.strength)
                                    selected_unit.set_buffs()
                                    print("Unit Health: {}".format(unit.health))
                                    selected_unit = None
                    else:
                        for unit in current_group.sprites():
                            if unit.rect.collidepoint(mouse_pos) and not burned_group.has(unit):
                                selected_unit = unit
            elif event.type == pygame.KEYDOWN:
                for key in key_dict:
                    if pygame.key.name(event.key) == key_dict[key].strip(' '):
                        if 'pass' in key.lower():
                            #code for turn passing
                            burned_group.empty()
                            selected_unit = None
                            current_mode = 'select'
                            if current_group == blue_group:
                                current_group_string = 'Orange'
                                current_group = orange_group
                                opposing_group = blue_group
                            elif current_group == orange_group:
                                current_group_string = 'Blue'
                                current_group = blue_group
                                opposing_group = orange_group
                        elif 'skip' in key.lower():
                            #code for skipping a unit
                            if selected_unit:
                                burned_group.add(selected_unit)
                                current_mode = 'select'
                                selected_unit = None
                        elif 'up' in key.lower():
                            #code for moving a unit up
                            if selected_unit:
                                if not burned_group.has(selected_unit):
                                    selected_unit.set_pos(selected_unit.rect.x,
                                                          selected_unit.rect.y - 70)
                                    if check_if_collide(current_group, opposing_group,
                                                        selected_unit):
                                        selected_unit.set_pos(selected_unit.rect.x,
                                                              selected_unit.rect.y + 70)
                                    else:
                                        burned_group.add(selected_unit)
                                        selected_unit = None
                        elif 'down' in key.lower():
                            #code for moving a unit down
                            if selected_unit:
                                if not burned_group.has(selected_unit):
                                    selected_unit.set_pos(selected_unit.rect.x,
                                                          selected_unit.rect.y + 70)
                                    if check_if_collide(current_group, opposing_group,
                                                        selected_unit):
                                        selected_unit.set_pos(selected_unit.rect.x,
                                                              selected_unit.rect.y - 70)
                                    else:
                                        burned_group.add(selected_unit)
                                        selected_unit = None
                        elif 'left' in key.lower():
                            #code for moving a unit to the left
                            if selected_unit:
                                if not burned_group.has(selected_unit):
                                    selected_unit.set_pos(selected_unit.rect.x + 70,
                                                          selected_unit.rect.y)
                                    if check_if_collide(current_group, opposing_group,
                                                        selected_unit):
                                        selected_unit.set_pos(selected_unit.rect.x - 70,
                                                              selected_unit.rect.y)
                                    else:
                                        burned_group.add(selected_unit)
                                        selected_unit = None
                        elif 'right' in key.lower():
                            #code for moving a unit to the right
                            if selected_unit:
                                if not burned_group.has(selected_unit):
                                    selected_unit.set_pos(selected_unit.rect.x - 70,
                                                          selected_unit.rect.y)
                                    if check_if_collide(current_group, opposing_group,
                                                        selected_unit):
                                        selected_unit.set_pos(selected_unit.rect.x + 70,
                                                              selected_unit.rect.y)
                                    else:
                                        burned_group.add(selected_unit)
                                        selected_unit = None
                        elif 'reset' in key.lower():
                            is_running = False
                            main_loop()
                        elif 'attack' in key.lower():
                            current_mode = 'attack'

        if burned_group.has(current_group):
            if current_group == blue_group:
                current_group_string = 'Orange'
                current_group = orange_group
                opposing_group = blue_group
            elif current_group == orange_group:
                current_group_string = 'Blue'
                current_group = blue_group
                opposing_group = orange_group
            selected_unit = None
            current_mode = 'select'
            burned_group.empty()
        for unit in blue_group.sprites():
            if unit.health <= 0:
                blue_group.remove(unit)
        for unit in orange_group.sprites():
            if unit.health <= 0:
                orange_group.remove(unit)
        window.fill((0, 204, 0))
        square_group = generate_gameboard(surface=window)
        orange_group.draw(window)
        blue_group.draw(window)
        pygame.display.update()
        if check_if_win(blue_group, orange_group):
            is_running = False

try:
    if 'server' in sys.argv[1]:
        print('x')
        multiplayer_loop(True)
    elif 'client' in sys.argv[1]:
        multiplayer_loop(False)
    elif 'about' in sys.argv[1]:
        print('CoronTorak v{}'.format(VERSION))
        print('Author: Chanku/Sapein')
        print("Artist: Dazzey")
        print("License: MIT (Code) || CC-BY-SA (Graphics)")
        print("")
        print("A simple Turn-Based-Strategy Game.")
        print("Originally written in VisualBasic.NET by Chanku/Sapein in three (3) weeks.")
        print("Ported to Python by Chanku/Sapein")
        print("Not compatible with original. Saving/Loading is not working")
        print("")

    elif sys.argv[1].lower() == '--help' or sys.argv[1].lower() == '--h' or sys.argv[1].lower() == 'help' or (
            sys.argv[1].lower() == 'h' or sys.argv[1].lower() == '-h'):
        print("This is the help menu!")
        print("[]: Required")
        print("(): Optional")
        print("Options:")
        print("help, h, -h, --h, --help: display this menu")
        print('server (port), --server (port): starts a CoronTorak MP Server. If a port is given, '
              'the server uses that port')
        print('client [ip] (port): connects to a CoronTorak MP Sever at the given address. If a '
              'port is given it connects on that port, otherwise it connects to the default port')
        print('about, --about: Displays the about text')
        print("")

except IndexError:
    main_loop()
pygame.quit()

